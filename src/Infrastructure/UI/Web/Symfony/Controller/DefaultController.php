<?php
namespace App\Infrastructure\UI\Web\Symfony\Controller;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment as Twig;

/**
 * @author Leonid Rusev <leonid.r@mserverone.com>
 */
class DefaultController
{
    private $twig;

    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    public function index()
    {
        $content = $this->twig->render('Default/index.html.twig');
        return new Response($content);
    }
}
