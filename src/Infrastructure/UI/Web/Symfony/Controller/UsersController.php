<?php
namespace App\Infrastructure\UI\Web\Symfony\Controller;

use App\Application\DeleteUserRequest;
use App\Application\NewUserRequest;
use App\Application\UserNotFoundException;
use App\Application\UsersService;
use App\Application\ValidationErrorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Twig\Environment as Twig;

class UsersController
{
    private $twig;
    private $usersService;

    public function __construct(UsersService $userService, Twig $twig)
    {
        $this->usersService = $userService;
        $this->twig = $twig;
    }

    public function index()
    {
        return $this->render(
            'Users/index-vue.html.twig',
            [
                'users' => $this->usersService->all(),
            ]
        );
    }

    public function create(Request $request, SerializerInterface $serializer)
    {
        if ($request->isMethod('POST')) {
            try {
                $newUserRequest = $serializer->deserialize($request->getContent(), NewUserRequest::class, 'json');
                $user = $this->usersService->createUser($newUserRequest);
            } catch (ValidationErrorException $exception) {
                return new JsonResponse(['success' => false, 'errors' => $exception->toArray()]);
            }

            return new JsonResponse(['success' => true, 'user' => $user], 201);
        }

        return $this->render('Users/create.html.twig', ['users' => $this->usersService->all()]);
    }

    public function delete($id)
    {
        try {
            $this->usersService->deleteUser(DeleteUserRequest::createFrom($id));
        } catch (UserNotFoundException $exception) {
            return new JsonResponse(['success' => false, 'error' => $exception->getMessage()]);
        }

        return new JsonResponse(['success' => true]);
    }

    private function render(string $template, array $data = array()): Response
    {
        $content = $this->twig->render($template, $data);
        return new Response($content);
    }
}