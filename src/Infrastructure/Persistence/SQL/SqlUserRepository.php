<?php
declare(strict_types=1);
namespace App\Infrastructure\Persistence\SQL;

use App\Domain\Model\User;
use App\Domain\Model\UserId;
use App\Domain\Model\UserRepository;

/**
 * @author Leonid Rusev <leonid.r@mserverone.com>
 */
class SqlUserRepository implements UserRepository
{
    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function ofId(UserId $id): ?User
    {
        $st = $this->execute('SELECT * FROM users WHERE id = :id', [
            'id' => $id->id()
        ]);

        if ($row = $st->fetch(\PDO::FETCH_ASSOC)) {
            return $this->buildUser($row);
        }

        return null;
    }

    public function ofEmail(string $email): ?User
    {
        $st = $this->execute('SELECT * FROM users WHERE email = :email', [
            'email' => $email
        ]);

        if ($row = $st->fetch(\PDO::FETCH_ASSOC)) {
            return $this->buildUser($row);
        }

        return null;
    }

    public function all(): array
    {
        return $this->retrieveAll('SELECT * FROM users');
    }

    private function retrieveAll(string $sql, array $parameters = []): array
    {
        $st = $this->pdo->prepare($sql);
        $st->execute($parameters);

        return array_map(
            function ($row) {
                return $this->buildUser($row);
            },
            $st->fetchAll(\PDO::FETCH_ASSOC)
        );
    }

    public function save(User $user)
    {
        $this->exist($user) ? $this->update($user) : $this->insert($user);
    }

    public function nextIdentity(): UserId
    {
        return new UserId();
    }

    private function exist(User $user)
    {
        $count = $this->execute('SELECT COUNT(*) FROM users WHERE id = :id', [':id' => $user->getId()->id()])
            ->fetchColumn();

        return $count == 1;
    }
    private function insert(User $user)
    {
        $sql = 'INSERT INTO users (id, name, email) VALUES (:id, :name, :email)';
        $this->execute($sql, [
            'id' => $user->getId()->id(),
            'name' => $user->getName(),
            'email' => $user->getEmail(),
        ]);
    }
    private function update(User $user)
    {
        $sql = 'UPDATE users SET name = :name, email = :email WHERE id = :id';
        $this->execute($sql, [
            'id' => $user->getId()->id(),
            'name' => $user->getName(),
            'email' => $user->getEmail(),
        ]);
    }
    private function execute($sql, array $parameters)
    {
        $st = $this->pdo->prepare($sql);
        $st->execute($parameters);

        return $st;
    }
    public function remove(User $user)
    {
        $this->execute('DELETE FROM users WHERE id = :id', [
            'id' => $user->getId()->id()
        ]);
    }

    private function buildUser(array $row)
    {
        return new User(
            new UserId($row['id']),
            $row['name'],
            $row['email']
        );
    }

    public function initSchema()
    {
        $this->pdo->exec(<<<SQL
CREATE TABLE IF NOT EXISTS users (
    id CHAR(36) PRIMARY KEY,
    name VARCHAR(250) NOT NULL,
    email VARCHAR(250) NOT NULL
)
SQL
        );
    }
}
