<?php
declare(strict_types=1);
namespace App\Domain\Model;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Leonid Rusev <leonid.r@mserverone.com>
 */
class User implements \JsonSerializable
{
    /**
     * @var UserId
     */
    private $id;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max=50)
     */
    private $name;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    public function __construct(UserId $id, string $name, string $email)
    {
        $this->id = $id;
        $this->setName($name);
        $this->setEmail($email);
    }

    public function getId(): UserId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id->id(),
            'name' => $this->name,
            'email' => $this->email,
        ];
    }
}
