<?php
namespace App\Domain\Model;

/**
 * @author Leonid Rusev <leonid.r@mserverone.com>
 */
interface UserRepository
{
    public function nextIdentity(): UserId;
    public function ofId(UserId $id): ?User;
    public function ofEmail(string $email): ?User;
    public function all(): array;
    public function save(User $user);
    public function remove(User $user);
}
