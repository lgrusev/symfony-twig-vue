<?php
namespace App\Application;

/**
 * @author Leonid Rusev <leonid.r@mserverone.com>
 */
class DeleteUserRequest
{
    /**
     * @var string
     */
    public $id;

    public static function createFrom(string $id)
    {
        $request = new self();
        $request->id = $id;

        return $request;
    }
}
