<?php
declare(strict_types=1);
namespace App\Application;

/**
 * @author Leonid Rusev <leonid.r@mserverone.com>
 */
class NewUserRequest
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $email;

    public static function createFrom(string $name, string $email)
    {
        $request = new self();
        $request->name = $name;
        $request->email = $email;

        return $request;
    }
}
