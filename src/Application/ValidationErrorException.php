<?php
declare(strict_types=1);
namespace App\Application;

use Exception;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * @author Leonid Rusev <leonid.r@mserverone.com>
 */
class ValidationErrorException extends Exception
{
    const DEFAULT_MESSAGE = 'Validation Error';

    /**
     * @var ConstraintViolationListInterface
     */
    private $errors;

    public function __construct(ConstraintViolationListInterface $errors, $code = 0, Exception $previous = null)
    {
        $this->errors = $errors;
        parent::__construct(self::DEFAULT_MESSAGE, $code, $previous);
    }

    public function toArray()
    {
        $errorsMap = [];
        foreach ($this->errors as $error) {
            $errorsMap[$error->getPropertyPath()] = $error->getMessage();
        }

        return $errorsMap;
    }
}
