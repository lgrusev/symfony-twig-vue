<?php
declare(strict_types=1);
namespace App\Application;

use App\Domain\Model\User;
use App\Domain\Model\UserId;
use App\Domain\Model\UserRepository;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @author Leonid Rusev <leonid.r@mserverone.com>
 */
class UsersService
{
    private $repository;
    private $validator;

    public function __construct(UserRepository $repository, ValidatorInterface $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    public function all()
    {
        return $this->repository->all();
    }

    public function createUser(NewUserRequest $request)
    {
        $user = new User(
            $this->repository->nextIdentity(),
            $request->name,
            $request->email
        );

        $this->validateUser($user);
        $this->repository->save($user);

        return $user;
    }

    /**
     * @param $user
     * @throws ValidationErrorException
     */
    private function validateUser(User $user)
    {
        $errors = $this->validator->validate($user);
        if ($errors === null || !$errors->count()) {
            return;
        }

        throw new ValidationErrorException($errors);
    }

    public function deleteUser(DeleteUserRequest $request)
    {
        $user = $this->repository->ofId(new UserId($request->id));
        if (!$user) {
            throw new UserNotFoundException("User not found");
        }

        $this->repository->remove($user);
    }
}
